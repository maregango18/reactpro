import React, { useState, useEffect } from 'react';
import Header from './components/header/Header';
import './App.css';
import Sidebar from './components/sidebar/Sidebar';
import Aside from './components/aside/Aside';
import Product from './components/catalog/Product';
import Catalog from './components/catalog/Catalog';

const App = () => {
  const [products, setProducts] = useState([]);
  const [sortType, setSortType] = useState('Sort By: New Arrivals');
  const [query, setQuery] = useState("");
  const [finalQuery, setFinalQuery] = useState();
  // const [productsClone, setProductsClone] = useState([]);
  //const [checkedProducts, setCheckedProducts] = useState(false);

  // const handleOnChange = () => {
  //   const checked = 
  //   // const updatedCheckedProducts = checkedProducts.map((item, index) =>
  //   //   index === position ? !item : item
  //   );
  //   setCheckedProducts(updatedCheckedProducts);
  // };


  const [count, setCount] = useState(0);
  const selectedCount = (value) => {
    setCount(count + value);
  };


  useEffect(() => {
    getProducts();
  }, []);

  useEffect(() => {
    const filterArray = [...products].filter((item) => {
      return item.title.toLowerCase().includes(finalQuery.toLowerCase())
    });
    setProducts(filterArray)
  }, [finalQuery]);



  const getProducts = async () => {
    const response = await fetch(
      "https://fakestoreapi.com/products"
    );
    const data = await response.json();
    setProducts(data);
    console.log(data);
  };
  useEffect(() => {
    const sortArray = type => {
      const types = {
        sortby: 'Sort By: New Arrivals',
        asc: 'Price: High To Low',
        desc: 'Price: Low To High',
        profH: 'Profit: High To Low',
        profL: 'Profit: Low To High',
      };
      let sorted = [];
      const sortProperty = types[type];
      if (sortProperty === "Sort By: New Arrivals") {
        sorted = [...products].sort((a, b) => (b.id > a.id) ? 1 : -1);
      } else if (sortProperty === "Price: High To Low") {
        sorted = [...products].sort((a, b) => b.price - a.price);
      } else if (sortProperty === "Price: Low To High") {
        sorted = [...products].sort((a, b) => a.price - b.price);
      } else if (sortProperty === "Profit: High To Low") {
        sorted = [...products].sort((a, b) => a.title.localeCompare(b.title));
      } else if (sortProperty === "Profit: Low To High") {
        sorted = [...products].sort((a, b) => b.title.localeCompare(a.title));
      }
      console.log(sorted);
      setProducts(sorted);
    };

    sortArray(sortType);
  },
    [sortType],
    [products],
    [setProducts]
  );

  const onChanged = (e) => setSortType(e.target.value);


  const changeQuery = (e) => {
    setQuery(e.target.value);
  }
  const getFinalQuery = (e) => {
    setFinalQuery(query);
    console.log(query);
  }
  return (
    <div className="App">
      <Sidebar />
      <Header query={query} onChange={onChanged} changeQuery={changeQuery} getFinalQuery={getFinalQuery} count={count} numberOfProducts={products.length} />
      <div className="catalog">
        {products && products.map(product => (
          <Product onChange={selectedCount}
            title={product.title}
            image={product.image}
            price={product.price}
          />
        ))}
      </div>
    </div>
  );
}

export default App;

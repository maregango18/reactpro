import React from 'react';
import './Aside.css';

function Aside() {
    return (
        <div class="sidenav">
            <select id="sorting">Choose Niche
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>
            <button class="dropdown_btn">Choose Category</button>
            <button class="dropdown_btn">Ship from</button>
            <button class="dropdown_btn">Ship to</button>
            <button class="dropdown_btn">Select Supplier</button>
            <button class="reset">Reset Filter</button>
        </div>
    );
}
export default Aside;
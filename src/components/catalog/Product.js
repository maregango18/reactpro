import React, { useState } from 'react';

const Product = ({ title, image, price, onChange }) => {
    const [selectedProducts, setSelectedProducts] = useState(false);

    const checkboxChanged = (e) => {
        const checked = e.target.checked;
        setSelectedProducts(checked);
        onChange(checked ? 1 : -1);
    }

    return (
        <div className="catalog__product">
            <input type="checkbox" onChange={checkboxChanged} checked={selectedProducts}></input>
            <div className="catalog__title"> {title}</div>
            <div class="catalog__photo">
                <img src={image} alt=" " />
            </div>
            <div class="catalog__prices"> {price}$ </div>
        </div>
    );
}

export default Product;
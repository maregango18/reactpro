import React, { useEffect, useState } from 'react';
import './Catalog.css';
import Product from "./Product";

const Catalog = ({ products }) => {

    // const [products, setProducts] = useState([]);

    // useEffect(() => {
    //     getProducts();
    // }, []);

    // const getProducts = async () => {
    //     const response = await fetch(
    //         "https://fakestoreapi.com/products"
    //     );
    //     const data = await response.json();
    //     setProducts(data);
    //     console.log(data);
    // };

    return (
        <div className="catalog">
            {products.map(product => (
                <Product
                    title={product.title}
                    image={product.image}
                    price={product.price}
                />
            ))}
        </div>
    );
};

export default Catalog;

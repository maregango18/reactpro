import React, { useState, useEffect } from 'react';
import Catalog from '../catalog/Catalog';

const Main = ({ products }) => {
    // const [products, setProducts] = useState([]);

    // useEffect(() => {
    //     getProducts();
    // }, []);

    // const getProducts = async () => {
    //     const response = await fetch(
    //         "https://fakestoreapi.com/products"
    //     );
    //     const data = await response.json();
    //     setProducts(data);
    //     console.log(data);
    // };
    return (
        <main className="main">
            <Catalog products={products} />
        </main>
    );
};

export default Main;
import React, { useState } from 'react';
import './Header.css';
import Button from './Button';
import Search from './Search';
import Sort from './Sort';

const Header = ({ query, onChange, changeQuery, getFinalQuery, count, numberOfProducts }) => {
    // const [query, setQuery] = useState("");
    // const [finalText, setFinalText] = useState('men');

    // const changeQuery = (e) => {
    //     setQuery(e.target.value);
    // }
    // const getFinalText = (e) => {
    //     setFinalText(query);
    //     console.log(query);
    // }


    return (
        <>
            <header className={"header"}>
                <div className={"header__content"}>
                    <Button content={"Select All"} />
                    <span className={"header__selection"}>
                        selected {count} out of {numberOfProducts} products
                    </span>
                    <Button content={"Clear"} />
                </div>
                <div className={"header__content"}>
                    <Search query={query} changeQuery={changeQuery} getFinalQuery={getFinalQuery} />
                    <Button content={"add to inventory"} />
                    <i class="far fa-question-circle header__questionmark"></i>
                </div>
            </header>
            <Sort onChange={onChange} />
        </>
    );
};

export default Header;
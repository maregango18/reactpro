import React, { useState, useEffect } from 'react';
import './Sort.css';

const Sort = ({ onChange }) => {


    return (
        <div className="sort__section">
            <i class="fas fa-sort-amount-up-alt" id="icon"></i>
            <select id="sort" onChange={onChange}>
                <option value="sortby">Sort By: New Arrivals </option>
                <option value="asc">Price: High To Low</option>
                <option value="desc">Price: Low To High</option>
                <option value="profH">Profit: High To Low</option>
                <option value="profL">Profit: Low To High</option>
            </select>
        </div>
    );
};

export default Sort;
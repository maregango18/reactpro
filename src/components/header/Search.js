import React, { useState } from 'react';
import Catalog from '../catalog/Catalog';
import Product from '../catalog/Product';
import './Search.css';

const Search = ({ changeQuery, getFinalQuery, query }) => {
    //const [query, setQuery] = useState("");
    // const [finalText, setFinalText] = useState();

    // const changeQuery = (e) => {
    //     setQuery(e.target.value);
    // }
    // const getFinalText = (e) => {
    //     setFinalText(query);
    //     console.log(query);
    // }

    return (
        <div classNameName={"search__content"}>
            <input
                id={"search"}
                className={"search__input"}
                type={"text"}
                placeholder={"Search"}
                value={query} onChange={changeQuery}
            />
            <button
                id={"search__button"}
                className={"header__searchbtn"}
                type={"submit"}
                onClick={getFinalQuery}
            >
                <i className="fas fa-search fa-rotate-90"></i>
            </button>
        </div>
    );
};
export default Search;


